// ==UserScript==
// @name         GGn Getchu Uploady
// @namespace    ggnGetchu
// @website      https://gitlab.com/__T__/ggn-scripts/
// @version      0.0.3
// @description  Uploady for Getchu
// @author       Tesp, based on VNDB Uploady by TNSepta
// @include      https://gazellegames.net/upload.php*
// @match        https://gazellegames.net/torrents.php?action=editgroup*
// @require      https://code.jquery.com/jquery-3.4.1.min.js
// @require      https://greasemonkey.github.io/gm4-polyfill/gm4-polyfill.js
// @connect      getchu.com
// @connect      postimages.org
// @connect      postimg.cc
// @grant        GM_xmlhttpRequest
// @grant        GM.xmlHttpRequest
// ==/UserScript==
/* globals $ */

//CONFIG VARIABLES
var AUTO_PTPIMG = 1; //1 for using PTPIMG, 0 for using PostImage
var TAGS_LOOKUP_TABLE = {
    "麻雀・ポンジャン": "mahjong",
    "戦争": "war",
    "寝取り": "netori",
    "寝取られ（NTR）": "netorare",
    "百合": "yuri",
    "輪○": "rape",
    "冒険": "adventure",
    "ホラー": "horror",
    "伝奇": "fantasy",
    "シリアス": "drama",
    "コメディ": "comedy",
    "恋愛": "romance",
    "ミステリー": "mystery"
}
//END CONFIG VARIABLES

var GGN_SPEC_TEXT = "\n\n[quote][align=center][b][u]System Requirements[/u][/b][/align]\n[b]Minimum[/b]\n[*][b]OS[/b]: \n[*][b]Processor[/b]: \n[*][b]Memory[/b]: \n[*][b]Storage[/b]:\n[/quote]"

// GGn specific text

var GETCHU_UPLOADY_FIELD =
    `<input placeholder="Getchu URI" type="text" id="getchu_uri" size="60">
     </input>`;

(function() {
    $("#categories").bind('click', function() {
        var el = $(this);
        setTimeout(function() {
            $("#getchu_uri").remove();
            if (el.find(":selected").text() == "Games") {
                insert_button(upload_page_handler);
            }
        }, 500);
    });
    //Insert the button by default.
    insert_button(upload_page_handler);
})();

function handle_page(handler, input) {
    return (response) => {
        if (response.status != 200) {
            $(input).val("No game found with that URL, or getchu is down.");
        }
        handler($(response.responseText));
    }
}

function insert_button(handler) {
    $("#categories").after($(GETCHU_UPLOADY_FIELD));
    $("#getchu_uri").on("blur", function() {
        $("#getchu_uri").val($(this).val());
        var input = this;
        var request = GM.xmlHttpRequest({
            "method": "GET",
            "url": $(this).val(),
            "cookie": "getchu_adult_flag",
            "onload": handle_page(handler, input)
        });
    });
}

function upload_page_handler(env) {
    $("#platform").val("Windows"); //Assume that VNs are Windows.
    $("#Rating").val(9); //Always set 18+ to prevent a warning.
    $("#aliases").val(get_aliases(env));
    $("#album_desc").val(get_desc(env) + GGN_SPEC_TEXT);
    $("#year").val(get_year(env));
    $("#tags").val("adult, visual.novel, " + get_tags(env));
    $("#image").val(get_cover(env));
    if ($("#image").val()) {
        postImageIt($("#image"));
    }
    set_screenshots(env)
}

function get_cover(env) {
    //Get the cover by url.
    var reg = /(?<=\?id=)[0-9]*/
    var id = reg.exec($('#getchu_uri').val());
    return ("http://www.getchu.com/brandnew/" + id + "/rc" + id + "package.jpg")
}

function set_screenshots(env) {
    var reg = /(?<=\?id=)[0-9]*/
    var id = reg.exec($('#getchu_uri').val());
    var len = env.find("img[src^='" + "./brandnew/" + id + "/c" + id + "sample']").length;
    // Make the screenshot field pretty
    if (len >= 16) {
        len = 16;
    }
    else if (len >= 12) {
        len = 12;
    }
    else if (len >= 8) {
        len = 8;
    }
    else if (len >= 4){
        len = 4;
    }

    // Add to screentshot field
    var add_screen = $("a:contains('+')");
    for (var i = 0; i < len; i++) {
        $("[name='screens[]']").eq(i).val("http://www.getchu.com/brandnew/" + id + "/c" + id + "sample" + (i + 1).toString() + ".jpg")
        postImageIt($("[name='screens[]']").eq(i))
        add_screen.click()
    }
}

function get_year(env) {
    // The year of THIS release
    if (env.find("a[title='同じ発売日の同ジャンル商品を開く']").length > 0) {
        return env.find("a[title='同じ発売日の同ジャンル商品を開く']").text().trim().split("/").shift();
    }
}

function get_aliases(env) {
    //The original title in Japanese
    return env.find("#soft-title").prop('firstChild').nodeValue.trim()
}


function get_tags(env) {
    //Grab all tags and concat them into a list. Translate into English using pre-defined hash.
    console.log(env.find("#soft_table").find("td:contains('カテゴリ')").next().find("a").toArray());
    var tags = env.find("#soft_table").find("td:contains('カテゴリ')").next().find("a").toArray().map(function(i) {
        return TAGS_LOOKUP_TABLE[i.innerText.trim()]
    });
    return tags.filter(
        function(el){
            return el != null
        }
    ).join(", ")
}

function get_desc(env) {
    //Grab description from page and prefill the system requirements template.
    if (env.find(".tabletitle_1").length > 0) {
        var desc = "\n[align=center][b][u]About the game[/u][/b]\n\n[/align]";
        desc += env.find(".tabletitle_1").next().text().trim();
        desc += ("\n\nGetchu link: " + $("#getchu_uri").val() + "\n");
        return desc
    };
}

// PostImage, since PTPImg cannot convert, credit to Abyraea

function postImageIt(input){
    var request = {
        method: 'GET',
        url: 'https://postimages.org/web/',
        onload: function(){
            console.log("start postIMG...");
            var token = /'token': ?'(\w+)'/.exec(this.responseText)[1];
            postIMGUpload(input, token);
        }
    };
    try{
        GM_xmlhttpRequest(request);
    } catch(err) {
        GM.xmlHttpRequest(request);
    };
};

function postIMGUpload(input, token){
    var data = '';
    data += 'token=' + token;
    data += '&upload_session=' + rand_string(32);
    data += '&numfiles=1';
    data += '&optsize=0';
    data += '&expire=1';
    data += '&url=' + encodeURIComponent(input.val());
    var request = {
        method: 'POST',
        url: 'https://postimages.org/json/rr',
        data: data,
        headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
        onload: function(){
            if(this.status === 200){
                console.log("image converted...");
                var data = JSON.parse(this.response);
                if(data.status === 'error'){
                    input.val(data.error);
                    console.log("error")
                }else{
                    postIMGFetch(input, data.url);
                };
            } else {
                input.val('Error: Maybe that link was bad or missing.');
            };
        }
    };
    try{
        GM_xmlhttpRequest(request);
    } catch(err) {
        GM.xmlHttpRequest(request);
    };
};

function postIMGFetch(input, url){
    var request = {
        method: 'GET',
        url: url,
        onload: function(){
            console.log("image fetching...");
            var parser = new DOMParser();
            var page = parser.parseFromString(this.responseText, 'text/html');
            input.val(page.querySelector('input#code_direct').value);
            // Then PTPIMG it
            if (AUTO_PTPIMG) {input.next().click()}
        }
    };
    try{
        GM_xmlhttpRequest(request);
    } catch(err) {
        GM.xmlHttpRequest(request);
    };
};

function rand_string(e) {
    for (var t = '', i = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789', n = 0; n < e; n++) t += i.charAt(Math.floor(Math.random() * i.length));
    return t
}
