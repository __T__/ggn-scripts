// ==UserScript==
// @name         GGn DLStie New Uploady
// @namespace    ggnDLSite
// @website      https://gitlab.com/__T__/ggn-scripts/
// @version      0.1.3
// @description  Uploady for DLSite
// @author       Tesp, based on VNDB Uploady by TNSepta
// @include      https://gazellegames.net/upload.php*
// @match        https://gazellegames.net/torrents.php?action=editgroup*
// @require      https://code.jquery.com/jquery-3.4.1.min.js
// @require      https://greasemonkey.github.io/gm4-polyfill/gm4-polyfill.js
// @connect      dlsite.com
// @grant        GM_xmlhttpRequest
// @grant        GM.xmlHttpRequest
// ==/UserScript==
/* globals $ */

//CONFIG VARIABLES
var AUTO_PTPIMG = 1;
var GENRES_LOOKUP_TABLE_JPN = {
    "コメディ": "comedy",
    "SF": "science.fiction",
    "格闘": "fighting",
    "シリアス": "drama",
    "ファンタジー": "fantasy",
    "ホラー": "horror",
    "バイオレンス": "violence",
    "寝取られ": "netorare",
    "百合": "yuri",
    "ミステリー": "mystery",
    "寝取り": "netori",
    "レイプ": "rape",
    "ロリ": "loli",
    "魔法少女": "mahou.shoujo",
}
var FORMAT_LOOKUP_TABLE_JPN = {
    "アクション": "action",
    "アドベンチャー": "adventure",
    "ロールプレイング": "role.playing.game",
    "シミュレーション": "simulation",
    "シューティング": "shooter",
    "パズル": "puzzle"
}
var GENRES_LOOKUP_TABLE_EN = {
    "Comedy": "comedy",
    "SF": "science.fiction",
    "Fighting/Martial Arts": "fighting",
    "Serious": "drama",
    "Fantasy": "fantasy",
    "Horror": "horror",
    "Violence": "violence",
    "Cuckoldry (Netorare)": "netorare",
    "Yuri/Girls Love": "yuri",
    "Mystery": "mystery",
    "Cuckoldry (Netori)": "netori",
    "R*pe": "rape",
    "Loli": "loli",
    "Magical Girl": "mahou.shoujo",
}
var FORMAT_LOOKUP_TABLE_EN = {
    "Action": "action",
    "Adventure": "adventure",
    "Role-playing": "role.playing.game",
    "Simulation": "simulation",
    "Shooting": "shooter",
    "Puzzle": "puzzle"
}
var GENRES_LOOKUP_TABLE_ZH_CN = {
    "喜剧": "comedy",
    "科幻": "science.fiction",
    "格斗": "fighting",
    "严肃": "drama",
    "幻想": "fantasy",
    "恐怖": "horror",
    "暴力": "violence",
    "被寝取/NTR": "netorare",
    "百合": "yuri",
    "推理": "mystery",
    "寝取": "netori",
    "强奸": "rape",
    "萝莉": "loli",
    "魔法少女": "mahou.shoujo",
}
var FORMAT_LOOKUP_TABLE_ZH_CN = {
    "动作": "action",
    "冒险": "adventure",
    "角色扮演": "role.playing.game",
    "模拟": "simulation",
    "射击": "shooter",
    "益智": "puzzle"
}
var GENRES_LOOKUP_TABLE_ZH_TW = {
    "喜劇": "comedy",
    "科幻": "science.fiction",
    "格鬥": "fighting",
    "嚴肅": "drama",
    "幻想": "fantasy",
    "恐怖": "horror",
    "暴力": "violence",
    "NTR": "netorare",
    "百合": "yuri",
    "推理": "mystery",
    "寢取": "netori",
    "強姦": "rape",
    "蘿莉": "loli",
    "魔法少女": "mahou.shoujo",
}
var FORMAT_LOOKUP_TABLE_ZH_TW = {
    "動作": "action",
    "冒險": "adventure",
    "角色扮演": "role.playing.game",
    "模擬": "simulation",
    "射擊": "shooter",
    "益智": "puzzle"
}
var GENRES_LOOKUP_TABLE_KO_KR = {
    "코메디": "comedy",
    "SF": "science.fiction",
    "격투": "fighting",
    "시리어스": "drama",
    "판타지": "fantasy",
    "호러": "horror",
    "바이올런스": "violence",
    "네토라레": "netorare",
    "백합": "yuri",
    "미스터리": "mystery",
    "네토리": "netori",
    "레이프": "rape",
    "로리": "loli",
    "마법 소녀": "mahou.shoujo",
}
var FORMAT_LOOKUP_TABLE_KO_KR = {
    "액션": "action",
    "어드벤쳐": "adventure",
    "롤플레잉": "role.playing.game",
    "시뮬레이션": "simulation",
    "슈팅": "shooter",
    "퍼즐": "puzzle"
}
// The English site dlsite.com/ecchi-eng/
var GENRES_LOOKUP_TABLE_EN_ECCHI = {
    "Comedy": "comedy",
    "SF": "science.fiction",
    "Fighting/Martial Arts": "fighting",
    "Serious": "drama",
    "Fantasy": "fantasy",
    "Horror": "horror",
    "Violence": "violence",
    "Cuckoldry (Netorare)": "netorare",
    "Yuri/Girls Love": "yuri",
    "Mystery": "mystery",
    "Cuckoldry (Netori)": "netori",
    "R*pe": "rape",
    "Loli": "loli",
    "Magical Girl": "mahou.shoujo",
    "Romance": "romance",
}
var FORMAT_LOOKUP_TABLE_EN_ECCHI = {
    "Action": "action",
    "Adventure": "adventure",
    "RPG": "role.playing.game",
    "Simulation": "simulation",
    "Shooting": "shooter",
    "Puzzle": "puzzle"
}
//END CONFIG VARIABLES

var DLSITE_UPLOADY_FIELD =
    `<input placeholder="DLSITE URI" type="text" id="dlsite_uri" size="60">
     </input>`;
var LANGUAGE = "ja-jp";

(function() {
    $("#categories").bind('click', function() {
        var el = $(this);
        setTimeout(function() {
            $("#dlsite_uri").remove();
            if (el.find(":selected").text() == "Games") {
                insert_button(upload_page_handler);
            }
        }, 500);
    });
    //Insert the button by default.
    insert_button(upload_page_handler);
})();

function handle_page(handler, input) {
    return (response) => {
        if (response.status != 200) {
            $(input).val("No game found with that URL, or dlsite is down.");
        }
        var language_reg = /(?<=html lang=")[a-z\-]*/
        LANGUAGE = language_reg.exec(response.responseText)[0]
        handler($(response.responseText));
    }
}

function insert_button(handler) {
    $("#categories").after($(DLSITE_UPLOADY_FIELD));
    $("#dlsite_uri").on("blur", function() {
        $("#dlsite_uri").val($(this).val());
        var input = this;
        var request = GM.xmlHttpRequest({
            "method": "GET",
            "url": $(this).val(),
            "cookie": 'adultchecked=1;locale=ja-jp;dctm_cookiecheck=1',
            "onload": handle_page(handler, input)
        });
    });
}

function upload_page_handler(env) {
    $("#platform").val("Windows"); //Assume that VNs are Windows.
    $("#Rating").val(9); //Always set 18+ to prevent a warning.
    $("#aliases").val(get_aliases(env));
    if (/^[\x00-\x7F]*$/.test($("#aliases").val())) {
        $("#title").val($("#aliases").val());
        $("#aliases").val("");
    }
    $("#album_desc").val(get_desc(env));
    $("#year").val(get_year(env));
    $("#tags").val("adult, visual.novel, " + get_tags(env));
    $("#image").val(get_cover(env));
    if ($("#image").val()) {
        $("#image").next().click();
    }
    set_screenshots(env)
}

function get_cover(env) {
    //Get the cover by url.
    if (env.find("img[src$='_img_main.jpg']").length > 0) {
        return "https:" + env.find("img[src$='_img_main.jpg']").attr('src');
    } else {
        return "";
    }
}

function set_screenshots(env) {
    var screenshot_prefix = env.find("img[src$='_img_main.jpg']").attr('src').slice(0,-8);
    var len = env.find("div[data-src^='" + screenshot_prefix + "smp']").length;

    // Make the screenshot field pretty
    if (len >= 16) {
        len = 16;
    }
    else if (len >= 12) {
        len = 12;
    }
    else if (len >= 8) {
        len = 8;
    }
    else if (len >= 4){
        len = 4;
    }
    // Add to screentshot field
    var add_screen = $("a:contains('+')");
    for (var i = 0; i < len; i++) {
        $("[name='screens[]']").eq(i).val("https:" + screenshot_prefix + "smp" + (i + 1).toString() + ".jpg")
        $("[name='screens[]']").eq(i).next().click()
        add_screen.click()
    }
}

function get_year(env) {
    // The year of THIS release
    switch (LANGUAGE)
    {
        case "ja-jp":
            if (env.find("#work_right_inner").find("th:contains('販売日')").length > 0) {
                return env.find("#work_right_inner").find("th:contains('販売日')").next().text().trim().split("年").shift();
            }
            break;
        case "en-us":
            if (env.find("#work_right_inner").find("th:contains('Release date')").length > 0) {
                return env.find("#work_right_inner").find("th:contains('Release date')").next().text().trim().split("/").pop();
            }
            break;
        case "zh-cn":
            if (env.find("#work_right_inner").find("th:contains('贩卖日')").length > 0) {
                return env.find("#work_right_inner").find("th:contains('贩卖日')").next().text().trim().split("年").shift();
            }
            break;
        case "zh-tw":
            if (env.find("#work_right_inner").find("th:contains('販賣日')").length > 0) {
                return env.find("#work_right_inner").find("th:contains('販賣日')").next().text().trim().split("年").shift();
            }
            break;
        case "ko-kr":
            if (env.find("#work_right_inner").find("th:contains('판매일')").length > 0) {
                return env.find("#work_right_inner").find("th:contains('판매일')").next().text().trim().split("년").shift();
            }
            break;
        case "en":
            if (env.find("#work_right_inner").find("th:contains('Release')").length > 0) {
                return env.find("#work_right_inner").find("th:contains('Release')").next().text().trim().split("/").pop();
            }
    }
}

function get_aliases(env) {
    //The original title in Japanese
    return env.find("#work_name")[0].innerText.trim();
}


function get_tags(env) {
    //Grab all tags and concat them into a list. Translate into English using pre-defined hash.
    var genres;
    var format;
    var tags;
    switch (LANGUAGE)
    {
        case "ja-jp":
            genres = env.find("#work_right_inner").find("th:contains('ジャンル')").next().find("a").toArray().map(function(i) {
                return GENRES_LOOKUP_TABLE_JPN[i.innerText.trim()]
            });
            format = env.find("#work_right_inner").find("th:contains('作品形式')").next().find("a").toArray().map(function(i) {
                return FORMAT_LOOKUP_TABLE_JPN[i.innerText.trim()]
            });
            tags = genres.concat(format);
            return tags.filter(function (val) {
                // remove empty values
                return val
            }).join(", ")
            break;
        case "en-us":
            genres = env.find("#work_right_inner").find("th:contains('Genre')").next().find("a").toArray().map(function(i) {
                return GENRES_LOOKUP_TABLE_EN[i.innerText.trim()]
            });
            format = env.find("#work_right_inner").find("th:contains('Product format')").next().find("a").toArray().map(function(i) {
                return FORMAT_LOOKUP_TABLE_EN[i.innerText.trim()]
            });
            tags = genres.concat(format);
            return tags.filter(function (val) {
                // remove empty values
                return val
            }).join(", ")
            break;
        case "zh-cn":
            genres = env.find("#work_right_inner").find("th:contains('分类')").next().find("a").toArray().map(function(i) {
                return GENRES_LOOKUP_TABLE_ZH_CN[i.innerText.trim()]
            });
            format = env.find("#work_right_inner").find("th:contains('作品类型')").next().find("a").toArray().map(function(i) {
                return FORMAT_LOOKUP_TABLE_ZH_CN[i.innerText.trim()]
            });
            tags = genres.concat(format);
            return tags.filter(function (val) {
                // remove empty values
                return val
            }).join(", ")
            break;
        case "zh-tw":
            genres = env.find("#work_right_inner").find("th:contains('分類')").next().find("a").toArray().map(function(i) {
                return GENRES_LOOKUP_TABLE_ZH_TW[i.innerText.trim()]
            });
            format = env.find("#work_right_inner").find("th:contains('作品形式')").next().find("a").toArray().map(function(i) {
                return FORMAT_LOOKUP_TABLE_ZH_TW[i.innerText.trim()]
            });
            tags = genres.concat(format);
            return tags.filter(function (val) {
                // remove empty values
                return val
            }).join(", ")
            break;
        case "ko-kr":
            genres = env.find("#work_right_inner").find("th:contains('장르')").next().find("a").toArray().map(function(i) {
                return GENRES_LOOKUP_TABLE_KO_KR[i.innerText.trim()]
            });
            format = env.find("#work_right_inner").find("th:contains('작품 형식')").next().find("a").toArray().map(function(i) {
                return FORMAT_LOOKUP_TABLE_KO_KR[i.innerText.trim()]
            });
            tags = genres.concat(format);
            return tags.filter(function (val) {
                // remove empty values
                return val
            }).join(", ")
            break;
        case "en":
            genres = env.find("#work_right_inner").find("th:contains('Genre')").next().find("a").toArray().map(function(i) {
                return GENRES_LOOKUP_TABLE_EN_ECCHI[i.innerText.trim()]
            });
            format = env.find("#work_right_inner").find("th:contains('Product format')").next().find("a").toArray().map(function(i) {
                return FORMAT_LOOKUP_TABLE_EN_ECCHI[i.innerText.trim()]
            });
            tags = genres.concat(format);
            return tags.filter(function (val) {
                // remove empty values
                return val
            }).join(", ")
            break;
    }
}

function get_desc(env) {
    //Grab description from page and prefill the system requirements template.
    var cpu_str = "";
    var memory_str = "";
    var storage_str = "";
    switch (LANGUAGE)
    {
        case "ja-jp":
            cpu_str = "CPU"
            memory_str = "メモリ"
            storage_str = "HDD"
            break;
        case "en-us":
            cpu_str = "CPU"
            memory_str = "Memory"
            storage_str = "HDD"
            break;
        case "zh-cn":
            cpu_str = "CPU"
            memory_str = "内存"
            storage_str = "HDD"
            break;
        case "zh-tw":
            cpu_str = "CPU"
            memory_str = "記憶體"
            storage_str = "HDD"
            break;
        case "ko-kr":
            cpu_str = "CPU"
            memory_str = "메모리"
            storage_str = "HDD"
            break;
        case "en":
            cpu_str = "CPU"
            memory_str = "Memory"
            storage_str = "HDD"
            break;
    }
    if (env.find(".work_article").length > 0) {
        var reg = /[a-zA-Z0-9 .\/®™]*/
        var desc = "\n[align=center][b][u]About the game[/u][/b][/align]\n\n";
        desc += ("DLSite link: " + $("#dlsite_uri").val() + "\n\n");
        desc += env.find(".work_article").first().text().trim();
        desc += ("\n\n[quote][align=center][b][u]System Requirements[/u][/b][/align]\n[b]Minimum[/b]" +
        "\n[*][b]OS[/b]: "+
        "\n[*][b]Processor[/b]: " + (env.find("dt:contains('" + cpu_str + "')").length > 0 ? reg.exec(env.find("dt:contains('" + cpu_str + "')").next().text())[0].trim() : "") +
        "\n[*][b]Memory[/b]: " + (env.find("dt:contains('" + memory_str + "')").length > 0 ? reg.exec(env.find("dt:contains('" + memory_str + "')").next().text())[0].trim() : "") +
        "\n[*][b]Storage[/b]: " + (env.find("dt:contains('" + storage_str + "')").length > 0 ? reg.exec(env.find("dt:contains('" + storage_str + "')").next().text())[0].trim() : "") +
        "\n[/quote]");
        return desc
    };
}
